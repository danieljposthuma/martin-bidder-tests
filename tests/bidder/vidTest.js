const axios = require('axios')
const data = require('./vidBid.json')

const BIDDER = 'http://localhost:3001/bids'

data.forEach(req => {
	axios.post(BIDDER, req)
	.then(resp => {
		console.log(resp)
	})
	.catch(err => {
		console.log(err)
	})
})
