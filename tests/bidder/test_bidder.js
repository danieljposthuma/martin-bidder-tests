const axios = require("axios")
const fs = require("fs")

const BIDDER = "http://localhost:3000/bids"

//const BIDDER = "https://dspeast.martin.ai/bids"

const GREEDY = "http://localhost:3050/greedy"

//const GREEDY = "http://104.197.141.212/greedy"

//const PLACEMENT_ID = "5d0a27d67f4dec0011b2d6c9"
const PLACEMENT_ID = '5d0a9182e336bd0012cec3a4'


const USER_ID = "catv"

//const USER_ID = "ML9evo2o9x"

//const data = fs.readFileSync("./banner.json")

const data = fs.readFileSync("./vid.json")

//const data = fs.readFileSync("./accuweather.json")

// console.log(vidRequest)

const bids = JSON.parse(data)


const greedyRequest = {
	placement_id: PLACEMENT_ID,
	user_id:      USER_ID,
}

const config = {

	//headers: { "Host": "bids-central.martin.ai" },
	//headers: { "Host": "dspeast.martin.ai" },
}

axios
	.post(GREEDY, greedyRequest)
	.then(resp => {
		console.log(resp.body)
	})
	.catch(err => {
		console.log(err)
	})

setTimeout(() => {
	setInterval(() => {
		axios
			.post(BIDDER, bids)
			.then(resp => {
		  console.log(JSON.stringify(resp.data, null, 2))
			})
			.catch(resp => {
				console.log(resp)
			})

	  //axios
		//.post(BIDDER, bids)
		//.then(resp => {
		  ////console.log(JSON.stringify(resp.data, null, 2));
		//})
		//.catch(resp => {
		  //console.log(resp);
		//});

		//axios
		//.post(BIDDER, bids)
		//.then(resp => {
		  ////console.log(JSON.stringify(resp.data, null, 2));
		//})
		//.catch(resp => {
		//console.log(resp);
		//});
	}, 0)
}, 2000)
