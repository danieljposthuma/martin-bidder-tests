const axios = require("axios");
const fs = require("fs");
const BIDDER = "http://localhost:3000/bids";
//const BIDDER = "https://bids-central.martin.ai/bids";
const GREEDY = "http://localhost:3050/greedy";
//const GREEDY = "http://104.197.141.212/greedy";

//const PLACEMENT_ID = "5cee8afab9563e0c53c9ef44"
const PLACEMENT_ID = "5cee8d10b9563e6f5cc9ef4a"

const USER_ID = "catv";

const banner = require("./banner.js")
const video = require("./video.js")


let greedyRequest = {
  placement_id: PLACEMENT_ID,
  user_id: USER_ID
};

axios
  .post(GREEDY, greedyRequest)
  .then(resp => {
	console.log(resp.body);
  })
  .catch(err => {
	console.log(err);
  });

setTimeout(() => {
  setInterval(() => {
      axios
        .post(BIDDER, banner)
        .then(resp => {
		  console.log(JSON.stringify(resp.data, null, 2));
        })
        .catch(resp => {
          console.log(resp);
        });
  }, 0);
}, 2000);
