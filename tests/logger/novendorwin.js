module.exports = {
	placementId:  '5cb89442157a794465858251',
	bidId:        '1234',
	impId:        '${AUCTION_IMP_ID}',
	priceCrypt:   '${AUCTION_PRICE:OXCRYPT}',
	price:         3000,
	dataCenter:   'undefined',
	cachebuster:   '{random}',
	domain:        'www.theeverygirl.com',
	page:          'https://www.theeverygirl.com/',
	geoCountry:   'USA',
	geoRegion:    'ny',
	geoMetro:     '501',
	geoCity:      'new york city',
	geoZip:       '11201',
	creativeId:   '5cb895f500d920454429089f',
	advertiserId: '5cb894c4f5b9cd44b9539a85',
	userId:       'catv'
}
