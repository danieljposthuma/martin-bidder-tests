module.exports = {
	placementId:  '5cb89442157a794465858251',
	bidId:        '1234',
	impId:        '1234',
	priceCrypt:   'abnconawfnpbn',
	price:         3000,
	dataCenter:   'undefined',
	cachebuster:   '{random}',
	domain:        'www.theeverygirl.com',
	page:          'https://www.theeverygirl.com/',
	geoCountry:   'USA',
	geoRegion:    'ny',
	geoMetro:     '501',
	geoCity:      'new york city',
	geoZip:       '11201',
	creativeId:   '5cb895f500d920454429089f',
	advertiserId: '5cb894c4f5b9cd44b9539a85',
	userId:       'catv',
	vendors:       [
		{
			_id:      '5ce3397df09eff010309069a',
			name:     'IAS',
			fee:      0.1,
			tier:     1,
			fee_type: 'flat',
		},
		{
			_id:      '5ce3397df09eff010309069b',
			name:     'Grapeshot',
			fee:      0.25,
			tier:     1,
			fee_type: 'flat',
		},
		{
			_id:      '5ce3397df09eff010309069c',
			name:     'Data Provider',
			fee:      1.25,
			tier:     1,
			fee_type: 'flat',
		},
		{
			_id:      '5ce3397df09eff010309069d',
			name:     'Platform Fee',
			fee:      0.11,
			tier:     2,
			fee_type: 'percent',
		},
		{
			_id:      '5ce3397df09eff010309069e',
			name:     'Managed Service',
			fee:      0.04,
			tier:     3,
			fee_type: 'percent',
		},
		{
			_id:      '5ce3397df09eff010309069f',
			name:     'Cleartrade',
			fee:      0.14,
			tier:     4,
			fee_type: 'percent',
		},
	],
}
