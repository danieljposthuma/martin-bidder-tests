const axios = require('axios')

let pass = 0
let fail = 0

setInterval(() => {
    axios.get("http://localhost:3000/", {
        timeout: 5000
    })
        .then(() => {
            pass++
            console.log("pass ->", pass)
            console.log("fail ->", fail)
        })
        .catch(() => {
            fail++
            console.log("pass ->", pass)
            console.log("fail ->", fail)
        })
}, 10000)
