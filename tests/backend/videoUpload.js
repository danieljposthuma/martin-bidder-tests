const request = require("request")
const fs = require("fs")

//const creativeId = "5d0a2851938f060018697405"
const creativeId = "5d0a70dce336bd0012cec298"


//const url = `http://localhost:3000/creative/upload/video/${creativeId}`;
const url = `https://bidder-backend.martin.ai/creative/upload/video/${creativeId}`
const filePath = "./videos/30summer-comp2.mp4"

const { size } = fs.statSync(filePath)
console.log(size)

fs.createReadStream(filePath).pipe(request.post(url))
	.on("response", (res) => {
		const body = []
		res.on("data", (chunk) => {
			body.push(chunk)
		})
		res.on("end", () => {
			const info = Buffer.concat(body).toString()
			console.log(info)
		})
		console.log(res.body)
	})
