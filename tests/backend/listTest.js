const fs = require("fs");
const axios = require("axios");
const BACKEND = "http://localhost:3000/list/upload/5cf6ba9d62412443b92cb025";
//const BACKEND = "https://bidder-backend.martin.ai/list/upload";

const dma_list = fs.readFileSync("./domain_list.csv");

let record = {
  name: "testing",
  values: JSON.stringify(dma_list),
  type: "domain",
  scope: "org",
  scope_id: "5cb893900f0b3243dcbba34b"
};

axios
  .put(BACKEND, record)
  .then(resp => {
    console.log(resp);
  })
  .catch(err => {
    console.log(err);
  });
