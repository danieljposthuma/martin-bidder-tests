const axios = require("axios");
const fs = require("fs");
const image = fs.readFileSync("../images/empowermm_300x250.jpg");

//empowermm_160x600.jpg empowermm_300x250.jpg empowermm_300x600.jpg empowermm_336x280.jpg

//const creativeId = "5cb89053b2d27ec6a48e064d"

const creativeId = "5cb85dc4491d7218ead2c661";

const url = `http://localhost:3000/creative/upload/${creativeId}`;
//const url = `https://bidder-backend.martin.ai/creative/upload/${creativeId}`

const upload = {
  image: JSON.stringify(image)
};

axios
  .post(url, upload)
  .then(success => {
    console.log(success.data);
  })
  .catch(err => {
    console.log(err);
  });
