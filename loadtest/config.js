"use strict"
const banner = JSON.stringify(require("./banner.js"))

module.exports.QPS = 3000
module.exports.DURATION = 1000 * 30 // 1 minute

module.exports.trustx = {
	hostname: "bids-central.martin.ai",
	family:   4,
	port:     80,
	path:     "/bids",
	method:   "POST",
	headers:  {
		"Content-Type":   "application/json",
		"Content-Length": banner.length,
		"Connection":     "keep-alive",
	},
}

module.exports.openx = {
	hostname: "dspeast.martin.ai",
	port:     80,
	path:     "/bids",
	method:   "POST",
	headers:  {
		"Content-Type":   "application/json",
		"Content-Length": banner.length,
		"Connection":     "keep-alive",
	},
}
