"use strict"
module.exports = {
	"site": {
		"publisher": {
			"id": "540021061",
		},
		"id":      "540021065",
		"domain":  "smallbusiness.chron.com",
		"cat":     ["IAB3"],
		"page":    "https://smallbusiness.chron.com/difference-between-list-price-net-price-35718.html",
		"mobile":  1,
		"content": { "len": 30, "language": "en" } },
	"id":   "ac693f9e-6563-4325-affc-6c0c6aa00a46",
	"at":   2,
	"tmax": 118,
	"imp":  [
		{
			"id":    "1",
			"video": {
				"ext": {
					"bcont": [
						1,
						8,
						15,
					],
					"matching_ad_id": [
						{
							"ad_width":     640,
							"ad_height":    360,
							"creative_id":  538296321,
							"placement_id": 123719133,
							"campaign_id":  537123013,
						},
					],
				},
				"h":        360,
				"w":        640,
				"api":      2,
				"delivery": [
					2,
				],
				"playbackmethod": [
					2,
					1,
					3,
				],
				"battr": [
					1,
					2,
					6,
					7,
				],
				"linearity":  1,
				"startdelay": 0,
				"protocols":  [
					1,
					4,
					2,
					5,
					3,
					6,
				],
				"maxduration": 30,
				"minduration": 0,
				"mimes":       [
					"application/javascript",
					"video/3gpp",
					"video/mp4",
				],
			},
			"displaymanager": "GOOGLE",
			"tagid":          "c21hbGxidXNpbmVzcy5jaHJvbi5jb201NDAwMjEwNzA0MDB4MjI1",
			"secure":         1,
			"exp":            3600,
		},
	],
	"user": {
		"id":       "b0f13a2a-cdef-4edc-9662-6b41d54e7d75",
		"buyeruid": "lTzTXjMrV",
	},
	"cur":    ["USD"],
	"source": {
		"fd":     1,
		"pchain": "6a698e2ec38604c6:540021061",
	},
	"device": {
		"ua":  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
		"geo": {
			"lat":       42.4775619506836,
			"lon":       -71.1804885864258,
			"type":      1,
			"country":   "USA",
			"region":    "MA",
			"metro":     "506",
			"city":      "burlington",
			"zip":       "01803",
			"utcoffset": 240,
			"ext":       {
				"is_test": 0,
			},
			"dnt":        0,
			"ip":         "144.121.20.0",
			"devicetype": 2,
			"make":       "desktop",
			"model":      "browser",
			"os":         "Windows",
			"osv":        "10",
			"pxratio":    1,
			"ext":        {
				"browser":         "Chrome",
				"browser_version": "74",
			},
		},
		"test": 0,
		"bcat": [
			"10",
			"26",
			"IAB17-18",
			"IAB25",
			"IAB7-39",
			"IAB9-9",
			"OX-10",
			"OX-26",
		],
		"regs": {
			"coppa": 0,
			"ext":   {
				"sb568": 0, "gdpr":  0,
			},
		},
	},
}
