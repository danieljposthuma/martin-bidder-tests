"use strict"
module.exports = {
	"user": {
		"buyeruid": "catv",
	},
	"site": {
		"publisher": {
			"id": "539936340",
		},
		"id":       "539936343",
		"domain":   "www.allrecipes.com",
		"cat":      ["IAB12"],
		"page":     "https://www.allrecipes.com/",
		"mobile":   1,
		"keywords": "si_section=none,apse={\"shouldSampleLatency\":false,\"chunkRequests\":false}",
	},
	"id":  "e739f74e-69cb-4007-b583-b7936c03fba1",
	"at":  2,
	"imp": [
		{
			"id":     "1",
			"banner": {
				"w":      336,
				"h":      280,
				"format": [
					{
						"w": 336,
						"h": 280,
					},
				],
				"ext": {
					"matching_ad_id": [
						{
							"ad_height":    336,
							"ad_width":     280,
							"campaign_id":  540258311,
							"placement_id": 540494076,
							"creative_id":  540514139,
						},
					],
				},
				"battr": [1, 2, 6, 7, 9, 10, 11, 14, 17],
			},
			"instl":  0,
			"tagid":  "dfp-ad-mid3_hp_web",
			"secure": 1,
			"exp":    336,
		},
	],
	"cur":    ["USD"],
	"source": {
		"fd":     1,
		"pchain": "6a698e2ec38604c6:539936340",
	},
	"device": {
		"ua":  "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763",
		"geo": {
			"lat":     41.849,
			"lon":     -87.671,
			"type":    2,
			"country": "USA",
			"region":  "ny",
			"city":    "new york city",
			"zip":     "11201",
			"ext":     {
				"continent": "north america",
				"dma":       501,
			},
		},
		"dnt":        0,
		"ip":         "73.72.67.48",
		"devicetype": 2,
		"make":       "desktop",
		"model":      "browser",
		"os":         "Windows",
		"osv":        "10",
		"h":          967,
		"w":          1920,
		"ext":        {
			"browser":         "Internet Edge",
			"browser_version": "18",
		},
	},
	"test": 0,
	"bcat": ["10", "12", "26", "34", "IAB11-3", "IAB11-4", "IAB14-1", "IAB15-5", "IAB17-18", "IAB22-1", "IAB23", "IAB25", "IAB7-17", "IAB7-39", "IAB7-44", "IAB7-5", "IAB9-9", "OX-10", "OX-12", "OX-26", "OX-34"],
	"regs": {
		"coppa": 0,
		"ext":   {
			"sb568": 0,
			"gdpr":  0,
		},
	},
}
