"use strict"
const cluster = require("cluster")
const os = require("os")
const config = require("./config.js")
const banner = JSON.stringify(require("./banner.js"))
const https = require("https")
const http = require("http")
const uniqid = require("uniqid")

const QPS = config.QPS
const DURATION = config.DURATION
let trustxResp = []
let errors = 0
let wins = 0
let bids = 0
let requests = 0

if (cluster.isMaster) {
	
	// Fork for CPUs
	const cpus = os.cpus().length
	const workers = []
	console.log(`Forking for ${cpus} CPUs`)
	for (let i = 0; i < cpus; i++) {

		// We need to capture references to the workers so we can send them refresh messages
		workers.push(cluster.fork())
	}
	
	// Handle a process crash by restarting a new one
	cluster.on("exit", (worker, code, signal) => {
		if (code !== 0 && !worker.exitedAfterDisconnect) {
			console.log(
				`Worker ${worker.id} crashed. ` + "Starting a new worker..."
			)
			cluster.fork()
		}
	})

} else {

	console.log("Starting test...")

	setInterval(() => {
		for (let i = 0; i < QPS / os.cpus().length; i++) {
			const trustxBanReq = http.request(config.trustx, (res) => {
				let body = ""
				res.on("data", (chunk) => {
					body += chunk
				})
				res.on("end", () => {
					requests++
					if (body.length > 0) {
						bids++
						trustxResp.push(JSON.parse(body))
					}
				})
			})

			trustxBanReq.on("error", (e) => {
				errors++
			})
			trustxBanReq.write(banner)
			trustxBanReq.end()
		}
	}, 1000)

	setInterval(() => {
		console.log("TRUSTX Bids -> ", bids)
		console.log("TRUSTX Wins -> ", wins)
		console.log("TRUSTX Errors -> ", errors)
		console.log("TRUSTX Requests -> ", requests)
		console.log()
	}, 5000)

	// winning every bid
	setInterval(() => {
		const requests = trustxResp
		trustxResp = []
		if (requests.length > 0) {
			requests.forEach(response => {

				let burl = response.seatbid[0].bid[0].burl
				burl = burl.replace("${AUCTION_ID}", "TEST")
				burl = burl.replace("${AUCTION_ID}", "TEST")
				burl = burl.replace("${AUCTION_PRICE}", "3")
				burl = burl.replace("${CACHEBUSTER}", uniqid())
				console.log(burl)
				https.get(burl, (res) => {
					const { statusCode } = res
					if (statusCode != 200) {
						errors++
					} else {
						wins++
					}
				})
			})
		}
	}, 1000)

	setTimeout(() => {
		console.log("TRUSTX Bids -> ", bids)
		console.log("TRUSTX Wins -> ", wins)
		console.log("TRUSTX Errors -> ", errors)
		console.log("TRUSTX Requests -> ", requests)
		console.log()
		process.exit(0)
	}, DURATION)
}
